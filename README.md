# Binary Document Upload

Thank you for connecting so far and taking up this coding challenge. 

In this task, the following skills are tested: 
1. Ability to use the MERN stack
2. Ability to use middlewares associated with NodeJS 
3. Ability to write and serve REST API endpoints 

User story for this task: 
As CO2OPT Recruiter, I would like to see an App where I can upload a binary file and have it stored in a database.  
The frontend shall call the endpoint implemented in the backend which in turn shall be responsible in document storage and retrieval. 

Your task is as follows: 
Frontend: 
1. Create a document upload Drop-Zone using React Drop-Zone. 
2. Use MaterialUI for styling 
3. Allow the drop zone to accept only PDF, PNG and JPG files. 

Backend: 
1. Implement a Create and Read endpoint for accepting the binary files POSTed from the front-end. 
2. Save the files in the local storage or in MongoDB (See BSON). 
3. Implement the code using MVC or the DDD architecture. 

References to use to speed up: 
1. https://react-dropzone.js.org/
2. https://www.mongodb.com/docs/manual/core/gridfs/


Note: 
Usage of docker will be a plus.
